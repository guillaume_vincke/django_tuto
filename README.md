## Setup postgresql:
~~~~~~~~~~sh
> psql
~~~~~~~~~~

~~~~~~~~~~sql
CREATE DATABASE polldb;
CREATE USER polladmin WITH PASSWORD 'polladmin';
ALTER ROLE polladmin SET client_encoding TO 'utf8';
ALTER ROLE polladmin SET default_transaction_isolation TO 'read committed';
ALTER ROLE polladmin SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE polldb TO polladmin;
\q
~~~~~~~~~~

## Ansible
Ansible requires an inventory file. Default location of that file is `/etc/ansible/hosts`.  
To use your own file, use `ansible -i [git_path]/ansible_hosts ...`. Please not that python has to be install on the host machine for ansible to be working

To deploy using ansible, use
~~~~~~~~~~sh
ansible-playbook -i ansible_hosts playbook.yml
~~~~~~~~~~
